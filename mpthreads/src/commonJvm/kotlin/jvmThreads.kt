package org.nexa.threads

import kotlinx.atomicfu.locks.withLock
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock

open class JvmThread(val thrd: java.lang.Thread): iThread
{
    override val name: String?
        get() = thrd.name
    override val id: ULong
        get() = thrd.id.toULong()

    override fun join() = thrd.join()
}

open class JvmMutex(override val name: String?): iMutex
{
    val mut = ReentrantLock()
    override fun <T> synchronized(block: () -> T): T
    {
        return mut.withLock(block)
    }


    override fun <T> ifavailable(block: () -> T): T?
    {
        val result = mut.tryLock()
        if (result)
        {
            try
            {
                return block()
            }
            finally
            {
                mut.unlock()
            }
        }
        return null
    }

    override fun <T> timedSync(milliSec: Long, block: () -> T): T?
    {
        val result = mut.tryLock(milliSec, TimeUnit.MILLISECONDS)
        if (result)
        {
            try
            {
                return block()
            }
            finally
            {
                mut.unlock()
            }
        }
        return null
    }

    override fun finalize()
    {
    }
}

open class JvmGate(override val name: String?): iGate, JvmMutex(name)
{
    val cond = mut.newCondition()

    override fun finalize()
    {
        super<JvmMutex>.finalize()
    }

    override fun <T> waitfor(condition: () -> Boolean, block: () -> T): T
    {
        mut.lock()
        try
        {
            while (true)
            {
                if (condition()) return block()
                cond.await()
            }
        }
        finally
        {
            mut.unlock()
        }
    }

    override fun <T> timedwaitfor(milliSec: Long, condition: () -> Boolean, block: () -> T): T?
    {
        val start = millinow()
        var result = mut.tryLock(milliSec, TimeUnit.MILLISECONDS)
        if (!result) return null
        try
        {
            while (true)
            {
                if (condition()) return block()
                val leftOver = milliSec - (millinow() - start)
                if (leftOver <= 0) return null
                result = cond.await(leftOver, TimeUnit.MILLISECONDS)
                if (!result) return null
            }
        }
        finally
        {
            mut.unlock()
        }
    }

    override fun loopwhile(exitCondition: () -> Boolean, block: () -> Unit)
    {
        mut.lock()
        try
        {
            while(exitCondition())
            {
                block()
                if (exitCondition()) cond.await()
            }
        }
        finally
        {
            mut.unlock()
        }
    }

    class LockWaiter(val lock: JvmGate):iGate.Waiter
    {
        override fun await():Unit
        {
            lock.cond.await()
        }

        override fun timedwait(milliSec: Long): Boolean
        {
            return lock.cond.await(milliSec, TimeUnit.MILLISECONDS)
        }
    }

    override fun <T> wlock(block: iGate.Waiter.() -> T): T
    {
        mut.lock()
        try
        {
            return LockWaiter(this).block()
        }
        finally
        {
            mut.unlock()
        }
    }

    override fun <T> wtrylock(block: iGate.Waiter.() -> T): T?
    {
        TODO("Not yet implemented")
    }

    override fun wake()
    {
        mut.withLock {
            cond.signalAll()
        }
    }

    /** Lock and execute a block of code, that presumably updates the state the other threads are waiting for.
     * Then wake those waiting threads. */
    override fun<T> wake(block: ()->T?):T?
    {
        return lock {
            val ret = block()
            cond.signalAll()
            ret
        }
    }

}

actual fun Thread(name: String?, thunk: () -> Unit): iThread
{
    val t = kotlin.concurrent.thread(start = true, name=name, block= {
        try
        {
            thunk()
        }
        catch(e:Exception)
        {
            DefaultThreadExceptionHandler?.invoke(e)
        }
    })
    return JvmThread(t)
}

actual fun Mutex(name: String?): iMutex = JvmMutex(name)

actual fun Gate(name: String?): iGate = JvmGate(name)

actual fun yield()
{
    java.lang.Thread.yield()
}

actual fun millisleep(milliSec: ULong)
{
    java.lang.Thread.sleep(milliSec.toLong())
}

actual fun microsleep(microSec: ULong)
{
    java.lang.Thread.sleep((microSec/1000UL).toLong(), (microSec%1000UL).toInt() *1000*1000)
}
