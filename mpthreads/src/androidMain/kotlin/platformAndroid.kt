package org.nexa.threads

actual fun platformName(): String = "Android ${android.os.Build.VERSION.RELEASE}: ${android.os.Build.VERSION.CODENAME} API:${android.os.Build.VERSION.RELEASE} ${android.os.Build.VERSION.SDK_INT}"
