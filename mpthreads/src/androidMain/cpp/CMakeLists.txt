cmake_minimum_required(VERSION 3.13.0)

project("mpthreads")

add_library(
        mpthreads
        SHARED
        nexathreads.cpp
    )

# target_include_directories(
#    mpthreads
#    PUBLIC ${CMAKE_CURRENT_LIST_DIR}/.....
# )

# set(NEXA_LIGHT "-Wl,--whole-archive ${CMAKE_CURRENT_LIST_DIR}/../../../../libnexa/build/android/${ANDROID_ABI}/libnexalight.a -Wl,--no-whole-archive")

target_link_libraries(mpthreads
        # ${NEXA_LIGHT}
        android
        log
        c++_static
)
