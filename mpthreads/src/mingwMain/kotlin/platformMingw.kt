package org.nexa.threads

import platform.windows.*
import platform.posix.*
import kotlinx.cinterop.*

private val threadLaunchControl = MswinGate("threadLauncher")
private var thunkToBeRun:(()->Unit)? = null

@OptIn(ExperimentalForeignApi::class)
class MswinThread(override val name:String?, val thid:pthread_t): iThread
{
    override val id: ULong
        get() = thid.toULong()

    override fun join()
    {
        val ret = pthread_join(thid, null)
        if (ret != 0) throw PlatformException("pthread_join error", ret)
    }

    protected fun finalize()
    {
        pthread_detach(thid)
    }
}

@OptIn(ExperimentalForeignApi::class)
class MswinGate(override val name:String?=null):iGate, MswinMutex(name)
{
    val pCond: pthread_cond_tVar = run {
        val mem = ms.alloc<pthread_cond_tVar>()
        val ret = pthread_cond_init(mem.ptr, null)
        if (ret != 0) throw PlatformException("pthread_cond_init error", ret)
        mem
    }

    override fun finalize()
    {
        pthread_cond_destroy(pCond.ptr)
        super<MswinMutex>.finalize()
    }

    class LockWaiter(val lock: MswinGate):iGate.Waiter
    {
        override fun await()
        {
            val ret = pthread_cond_wait(lock.pCond.ptr, lock.pMutex.ptr)
            if (ret != 0) throw PlatformException("pthread_cond_wait error", ret)
        }

        override fun timedwait(milliSec: Long): Boolean
        {
            val abstime = millinow() + milliSec
            memScoped {
                val tm = this.alloc<timespec>()
                tm.tv_sec = abstime / 1000
                tm.tv_nsec = (abstime % 1000).toInt() * 1000 * 1000
                val ret = pthread_cond_timedwait(lock.pCond.ptr, lock.pMutex.ptr, tm.ptr)
                if (ret == ETIMEDOUT)
                {
                    return false
                }
                if (ret != 0) throw PlatformException("pthread_mutex_timedlock error", ret)
            }
            return true
        }
    }

    override fun<T> waitfor(condition: ()->Boolean, block: () -> T): T
    {
        var ret = pthread_mutex_lock(pMutex.ptr)
        if (ret != 0) throw PlatformException("pthread_mutex_lock error", ret)
        try
        {
            while (true)
            {
                if (condition()) return block()
                ret = pthread_cond_wait(pCond.ptr, pMutex.ptr)
                if (ret != 0) throw PlatformException("pthread_cond_wait error", ret)
            }
        }
        finally
        {
            ret = pthread_mutex_unlock(pMutex.ptr)
            if (ret != 0) throw PlatformException("pthread_mutex_unlock error", ret)
        }
    }

    override fun <T> timedwaitfor(milliSec: Long, condition: () -> Boolean, block: () -> T): T?
    {
        val start = millinow()
        val abstime = start + milliSec
        memScoped {
            val tm = this.alloc<timespec>()
            tm.tv_sec = abstime / 1000
            tm.tv_nsec = (abstime % 1000).toInt() * 1000 * 1000
            var ret = pthread_mutex_timedlock(pMutex.ptr, tm.ptr)
            if (ret == ETIMEDOUT) return null
            if (ret != 0) throw PlatformException("pthread_mutex_lock error", ret)
            try
            {
                while (true)
                {
                    if (condition()) return block()
                    var elapsed = millinow() - start
                    if (elapsed > milliSec)
                    {
                        return null
                    }
                    ret = pthread_cond_timedwait(pCond.ptr, pMutex.ptr, tm.ptr)
                    if (ret == ETIMEDOUT)
                    {
                        return null
                    }
                    if (ret != 0) throw PlatformException("pthread_cond_wait error", ret)
                }
            }
            finally
            {
                ret = pthread_mutex_unlock(pMutex.ptr)
                if (ret != 0) throw PlatformException("pthread_mutex_unlock error", ret)
            }
        }
        return null
    }

    override fun loopwhile(exitCondition: ()->Boolean, block: () -> Unit): Unit
    {
        var ret = pthread_mutex_lock(pMutex.ptr)
        if (ret != 0) throw PlatformException("pthread_mutex_lock error", ret)
        try
        {
            while(exitCondition())
            {
                block()
                val pret = pthread_cond_wait(pCond.ptr, pMutex.ptr)
                if (pret != 0) throw PlatformException("pthread_cond_wait error", pret)
            }
        }
        finally
        {
            ret = pthread_mutex_unlock(pMutex.ptr)
            if (ret != 0) throw PlatformException("pthread_mutex_unlock error", ret)
        }
    }

    override fun <T> wlock(block: iGate.Waiter.() -> T): T
    {
        val ret = pthread_mutex_lock(pMutex.ptr)
        if (ret != 0) throw PlatformException("pthread_mutex_lock error", ret)
        try
        {
            return LockWaiter(this).block()
        }
        finally
        {
            val retu = pthread_mutex_unlock(pMutex.ptr)
            if (retu != 0) throw PlatformException("pthread_mutex_unlock error", ret)
        }
    }
    override fun <T> wtrylock(block: iGate.Waiter.() -> T): T?
    {
        TODO("Not yet implemented")
    }
    override fun wake()
    {
        var ret = pthread_cond_broadcast(pCond.ptr)
        if (ret != 0) throw PlatformException("pthread_cond_broadcast error", ret)
    }
}

@OptIn(ExperimentalForeignApi::class)
open class MswinMutex(override val name:String?=null):iMutex
{
    val ms = MemScope()
    val pMutex: pthread_mutex_tVar = run {
        val carr = ms.alloc<pthread_mutex_tVar>()
        val attr = ms.alloc<pthread_mutexattr_tVar>()
        pthread_mutexattr_init(attr.ptr)
        pthread_mutexattr_settype(attr.ptr, PTHREAD_MUTEX_RECURSIVE.toInt())
        val ret = pthread_mutex_init(carr.ptr, attr.ptr)
        pthread_mutexattr_destroy(attr.ptr)
        if (ret != 0) throw PlatformException("pthread_mutex_init error", ret)
        carr
    }
    override fun finalize()
    {
        pthread_mutex_destroy(pMutex.ptr)
    }

    override fun <T> synchronized(block: (() -> T)): T
    {
        val ret = pthread_mutex_lock(pMutex.ptr)
        if (ret != 0) throw PlatformException("pthread_mutex_lock error", ret)
        try
        {
            return block()
        }
        finally
        {
            val retu = pthread_mutex_unlock(pMutex.ptr)
            if (retu != 0) throw PlatformException("pthread_mutex_unlock error", ret)
        }
    }

    override fun <T> ifavailable(block: (() -> T)): T?
    {
        val ret = pthread_mutex_trylock(pMutex.ptr)
        if (ret != 0)
        {
            if (ret == EBUSY) return null // lock taken
            throw PlatformException("pthread_mutex_lock error", ret)
        }
        try
        {
            return block()
        }
        finally
        {
            val retu = pthread_mutex_unlock(pMutex.ptr)
            if (retu != 0) throw PlatformException("pthread_mutex_lock error", ret)
        }
    }

    override fun <T> timedSync(milliSec: Long, block: (() -> T)): T?
    {
        val abstime = millinow() + milliSec
        memScoped {
            val tm = this.alloc<timespec>()
            tm.tv_sec = abstime / 1000
            tm.tv_nsec = (abstime % 1000).toInt() * 1000 * 1000
            val ret = pthread_mutex_timedlock(pMutex.ptr, tm.ptr)
            if (ret == ETIMEDOUT)
                return null
            if (ret != 0) throw PlatformException("pthread_mutex_timedlock error", ret)
        }
        try
        {
            return block()
        }
        finally
        {
            val retu = pthread_mutex_unlock(pMutex.ptr)
            if (retu != 0) throw PlatformException("pthread_mutex_lock error", retu)
        }
    }
}


actual fun platformName():String
{
    return "MS Windows " + VER_MAJORVERSION + "." + VER_MINORVERSION + "." + VER_BUILDNUMBER
}

actual fun Gate(name:String?):iGate = MswinGate(name)

actual fun Mutex(name:String?): iMutex = MswinMutex(name)

@OptIn(ExperimentalForeignApi::class)
actual fun Thread(name:String?, thunk: ()->Unit) : iThread
{
    memScoped {

        val fn = staticCFunction<COpaquePointer?,COpaquePointer?>(
            {
                var tmp: (() -> Unit)? = null
                threadLaunchControl.wake {
                    tmp = thunkToBeRun
                    thunkToBeRun = null
                }
                try
                {
                    tmp?.invoke()
                }
                catch(e: Exception)
                {
                    DefaultThreadExceptionHandler?.invoke(e)
                }
                null
            }
        )

        val carr = alloc<pthread_tVar>()
        threadLaunchControl.wlock {
            while (thunkToBeRun != null) this.await()
            carr.value = 0UL
            thunkToBeRun = thunk
            val ret = pthread_create(carr.ptr, null, fn, null)
            if (ret != 0)
                throw PlatformException("error in pthread_create", ret)
        }
        val tmp = carr.value
        if (tmp == null)
            throw PlatformException("error in pthread_create -- pthread_t is null", 0)
        return MswinThread(name, tmp)
    }
}

actual fun yield()
{
    val ret = sched_yield()
    if (ret == -1) throw PlatformException("pthread_yield error", errno)
}

actual fun millisleep(milliSec: ULong)
{
    usleep((milliSec * 1000UL).toUInt())
}
actual fun microsleep(microSec: ULong)
{
    usleep(microSec.toUInt())
}
