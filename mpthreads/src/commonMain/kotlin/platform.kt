package org.nexa.threads

import kotlinx.datetime.Clock

class PlatformException(reason:String, val errno: Int):Exception(reason)

//@Target(AnnotationTarget.FIELD, AnnotationTarget.TYPE)
//annotation class LockedBy(val mutex: Mutex)

/** Install a generic catch-all exception handler for any threads */
var DefaultThreadExceptionHandler: ((Exception) -> Unit)? = {
    println(it)
    it.printStackTrace()
}

interface iThread
{
    val name: String?
    val id: ULong
    fun join():Unit

    //fun sleep()
}

/** A mutex (mutual-exclusion) object ensures that code executed within the blocks passed into its member functions
 * is executed atomically with respect to all code (potentially running in other threads) that is also run within similar blocks.
 *
 * Eliding the details: use this if you need multiple operations to appear to execute all together.
 */
interface iMutex
{
    val name: String?

    /** Locks the mutex during the execution of the provided code block.  All code within the block executes atomically from the perspective
     * of any other entities that also use this function.
     * @synonym synchronized
     * @return Whatever the passed code block returns
     * */
    fun<T> lock (block: (()->T)): T = synchronized(block)

    /** Locks the mutex during the execution of the provided code block.  All code within the block executes atomically from the perspective
     * of any other entities that also use this function.
     * @synonym lock
     * @return Whatever the passed code block returns
     * */
    fun<T> synchronized(block: (()->T)): T

    /** If the mutex is available, this function behaves as lock.  Otherwise it returns null and does not execute the provided code block.
     * Note that if your block can return null, the return value may be ambiguous.
     * @synonym trylock
     * @return null if the lock could not be taken, otherwise the result of the executed code block
     */
    fun<T> ifavailable(block: (()->T)): T?

    /** If the mutex is available, this function behaves as lock.  Otherwise it returns null and does not execute the provided code block.
     * Note that if your block can return null, the return value may be ambiguous.
     * @synonym ifavailable
     * @return null if the lock could not be taken, otherwise the result of the executed code block
     */
    fun<T> trylock(block: (()->T)): T? = ifavailable(block)


    /** Wait for [milliSec] to see if the mutex becomes available.  If so this function behaves as lock.  Otherwise it returns null and does not execute the provided code block.
     * Note that if your block can return null, the return value may be ambiguous.
     * Note that some platforms do not support this functionality at the OS level.  In this case, a trylock/delay loop is used.
     * @synonym timedSync
     * @return null if the lock could not be taken, otherwise the result of the executed code block
     */
    fun<T> timedSync(milliSec:Long, block: (()->T)): T?

    /** Wait for [milliSec] to see if the mutex becomes available.  If so this function behaves as lock.  Otherwise it returns null and does not execute the provided code block.
     * Note that if your block can return null, the return value may be ambiguous.
     * Note that some platforms do not support this functionality at the OS level.  In this case, a trylock/delay loop is used.
     * @synonym timedSync
     * @return null if the lock could not be taken, otherwise the result of the executed code block
     */
    fun<T> timedlock(milliSec:Long, block: (()->T)): T? = timedSync(milliSec, block)
    fun finalize()
}

/** A Gate allows threads to synchronize activity (traditionally called a thread "Condition").
 * Threads may "wait" (block) for another thread to "wake" them.
 * Note that there is some trickiness there -- if a thread isn't waiting EXACTLY when another thread calls wake it might "miss" the wake.
 * And N "wake" calls do not imply actually waking from "wait" N times.
 * Instead, expect spurious wakes (check the actual condition you are waiting for upon wake), and consume ALL available state
 * when woken.
 */
interface iGate:iMutex
{
    interface Waiter
    {
        /* Wait for another thread to wake this one up */
        fun await():Unit

        /* Executes the thunk if the timeout occurs */
        fun timedwait(milliSec: Long): Boolean
    }

    override val name: String?

    /** Lock the mutex and execute condition.  If true, execute the block (with the mutex still locked), unlock and return the result.
     * If the condition is not true, block and repeat when woken up.
     */
    fun<T> waitfor(condition: ()->Boolean, block: () -> T): T

    /** As waitfor, but limit the time that the code blocks waiting for the condition to be true.
     * Details: Lock the mutex and execute condition.  If true, execute the block (with the mutex still locked), unlock and return the result.
     * If the condition is not true, block (for cumulatively no less than [milliSec] milliseconds), and repeat when woken up.
     * @return null on timeout, or the result of executing the code block
     */
    fun<T> timedwaitfor(milliSec: Long, condition: ()->Boolean, block: () -> T): T?

    /** Delay until the condition is true, for at least a certain amount of time.  Whenever the Gate is released, all waiting threads will
     * reevaluate their condition.
     * @param milliSec this function will wait for the condition to be true for at least this amount of time
     * @param condition a condition function; return true to stop waiting
     * @return true if the condition became true (note, do not RELY on the condition being true upon return -- for that use timedwaitfor and pass a block), false if the function timed out.
     * */
    fun delayuntil(milliSec: Long, condition: ()->Boolean):Boolean
    {
        var ret = timedwaitfor(milliSec, condition, { true })
        return ret ?: false
    }

    /** Lock the mutex and loop until the passed [exitCondition] is true.  If false, execute the block, wait, then repeat until [exitCondition].
     * This is a convenience function that captures the loop within the lock.  In other words this is not great code:
     * while(something)
     * {
     *    waitfor({ /* check something */}, { /* do something */ })
     * }
     * There is a moment during every loop when the lock is not taken, during the evaluation of "something".  At that moment another thread might call "wake".  That wake will
     * be missed because this thread is not waiting at that exact moment.
     * You can fix this code by wrapping it in a lock, but loopwhile captures all of that cleanly.
     */
    fun loopwhile(exitCondition: ()->Boolean, block: () -> Unit): Unit

    /** Lock the mutex, and provide the passed code block with wait and timedwait primitives.
     * */
    fun<T> wlock(block: (Waiter.()->T)): T

    /** Lock the mutex if available, and provide the passed code block with wait and timedwait primitives.
     * */
    fun<T> wtrylock(block: (Waiter.()->T)): T?

    /** Wake up any threads that might be "wait"-ing.
     * To write perfectly correct code (that is, code that actually wakes every time you think it should), it is strongly recommended
     * to use this function inside a "lock" block.
     * If it is called outside of a lock, wake events might appear to be missed.  For example, the other thread might be executing the block
     * at the moment wake is called, rather than waiting.  Since the other thread is already awake, calling this function does nothing.
     * The other thread then finishes and goes back into "wait".  You then wonder why it didn't wake up due to the "next" event coming in!
     *
     * However, if your application tolerates occasionally missing a wake, calling this outside of the lock allows greater parallelism.
     * */
    fun wake()

    /** Lock and execute a block of code, that presumably updates the state the other threads are waiting for.
     * Then wake those waiting threads. */
    fun<T> wake(block: ()->T?):T?
    {
        return lock {
            val ret = block()
            wake()
            ret
        }
    }

    override fun finalize()
}

interface Platform
{
    val name: String

}

/** Launches a thread executing the passed function */
expect fun Thread(name:String?=null, thunk: ()->Unit):iThread

/** Mutual exclusion (atomicity): Allows you to execute multiple steps as if they
* were a single step from the perspective of other entities using this same object. */
expect fun Mutex(name:String?=null):iMutex

/** Gate: Allows you to make threads wait for other threads to tell them to go.
 * Known in classical threading literature as a thread "Condition" */
expect fun Gate(name:String?=null):iGate

/** Let some other thread run:  Its a terrible idea to use this API except for testing, because some particular other thread is not guaranteed to run, or to get to where you wanted before this one starts back up. */
expect fun yield()

/** Pause this thread: ONLY use this API to actually delay things, for example, in a retry loop trying to access some resource.
 * Its a terrible idea to use this API to let some other thread get something done.  Instead use a Gate. */
expect fun millisleep(milliSec: ULong)


/** Pause this thread: ONLY use this API to actually delay things, for example, in a retry loop trying to access some resource.
 * Its a terrible idea to use this API to let some other thread get something done.  Instead use a Gate. */
expect fun microsleep(microSec: ULong)


/** Return what OS and/or architecture you are running on.
 *  Ok this is not really a threading thing; but with all the different simulators out there, it gets insanely confusing without something like this.
 *  The format of the string is undefined -- don't parse it.  If you need to programatically discover your platform, make your own API!
 */
expect fun platformName(): String

fun millinow():Long = Clock.System.now().toEpochMilliseconds()