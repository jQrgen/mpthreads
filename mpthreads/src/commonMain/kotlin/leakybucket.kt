package org.nexa.threads
import kotlin.time.TimeSource.Monotonic

/** A leaky bucket is a simple integer access/traffic shaper.  Imagine a bucket that is being filled at a constant rate,
 * but cannot exceed some volume.  You may periodically ask to remove some liquid from the bucket.  If there is not enough liquid
 * your request will be denied.
 *
 * Programmers should "tie" the amount requested to be removed to the resource they want to shape.  For example each iota of liquid
 * could represent a byte over a communications channel, or a connection attempt.
 *
 * This allows you to define an average rate of removal, yet allow some burstiness.
 *
 * The leaky bucket starts upon construction, but you can stop/restart it.
 * If stopped, the bucket behaves like it has an infinite amount (no take calls block).  This helps thread cleanup while ending applications.
 */
class LeakyBucket(
    /** Maximum value this bucket can contain. */
    var max: Long,
    /** Fill rate in units/sec.  If you need to fill at less than 1 per sec, redefine your unit. */
    var fill: Long,
    /** Current (and starting) level of the bucket .*/
    var level:Long = 0)
{
    var done = false
    val prot = Gate()
    var lastFill = Monotonic.markNow()
    var prior = lastFill
    protected fun fillIt()
    {
        prot.lock {
            if (level != Long.MAX_VALUE)  // this is a trigger to pause filling
            {
                val elapsed = prior.elapsedNow().inWholeMilliseconds
                if (elapsed > 50)
                {
                    prior = Monotonic.markNow()
                    val fillElapsed = (prior-lastFill).inWholeMilliseconds // figure out the exact time thats passed since last fill
                    val fillAmt = (fill * fillElapsed) / 1000
                    if (fillAmt > 0)  // Dont trigger if we haven't managed to fill by at least 1, or we may never get enough to do so
                    {
                        level += fillAmt
                        lastFill = prior // since we can actually fill, set the fill time to now
                        if (level > max) level = max
                    }
                }
            }
        }
    }

    /** Stop this leaky bucket.  All APIs will stop blocking */
    fun stop() { done = true }

    /** Restart this leaky bucket. */
    fun restart()
    {
        lastFill = Monotonic.markNow()
        prior = lastFill
        done = false
    }

    /*  not really needed because I can fill only when a thread is trying to leak
    suspend fun fillForever(periodMs: Long = 250)
    {
        while (!done)
        {
            fillIt()
            delay(periodMs)
        }
    }
     */

    /** Consumes [amt] tokens.  Block until they are available
     *  If the leaky bucket is off, this function returns right away
     */
    fun take(amt: Long)
    {
        if (done) return // leaky bucket is turned off.
        if (amt > max) throw IllegalArgumentException("Leaky bucket infinite wait: taking ($amt) more than max ($max).")
        while (null == prot.timedwaitfor(100,
                {
                    (level >= amt) || (level == Long.MAX_VALUE)
                },
                {
                    level -= amt; true
                }))
            fillIt()
    }

    /** Try to consume [amt] tokens.
     * Returns TRUE if the tokens were consumed, false otherwise
     * If the leaky bucket is off, this function returns true right away
     */
    fun<T> trytake(amt: Long, thunk: ()->T):T?
    {
        val avail = prot.lock {
            if (done) true // leaky bucket is turned off.
            else
            {
                fillIt()
                if (level >= amt)
                {
                    level -= amt
                    true
                }
                else false
            }
        }
        if (avail) return thunk()
        return null
    }

}