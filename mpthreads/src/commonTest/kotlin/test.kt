import org.nexa.threads.*
import kotlin.concurrent.Volatile
import kotlin.test.*
import kotlin.time.TimeSource

// DO NOT RUN ANDROID TESTS from commonTest.  Run them from androidInstrumentedTest!
class CommonTests
{
    @Test
    fun test()
    {
        val p = platformName()
        check(p.isNotEmpty())
        println("Common tests on: " + p)
    }

    @Test
    fun testThreads()
    {
        println("thread test")
        var cnt = 0
        val q = mutableListOf<Long>(0)
        val th = Thread {
            // println("thunk is running")
            cnt += 1
            q.add(cnt.toLong())
        }
        th.join()
        println(cnt)
        check(cnt == 1)
        check(q.size == 2)   // check appending to a list in the thread
    }

    @Test fun testThreadException()
    {
        var cnt = 0
        DefaultThreadExceptionHandler = {
            cnt++
        }
        Thread {
            throw Exception("test")
        }.join()
        check(cnt == 1)
        // Set it back to something that is very unhappy when an exception happens for the tests
        DefaultThreadExceptionHandler = { println(it); it.printStackTrace(); throw it }
    }

    @Test fun testRecursiveMutex()
    {
        println("testRecursiveMutex")
        val mutex = Mutex()
        var a = 0
        mutex.lock {
            mutex.lock {
                a++
            }
        }
    }
    @Test
    fun testMutex()
    {
        println("testMutex")
        val mutex = Mutex()

        var cnt = 0
        var cnt2 = 0
        val th = Thread {
            // println("thunk is running")
            for (i in 1..10000) mutex.lock {
                cnt = cnt + 1
                yield()
                cnt2 = cnt2 + 1
            }

        }
        for (i in 1..10000) mutex.lock {
            cnt = cnt + 1
            yield()
            cnt2 = cnt2 + 1
            }
        th.join()
        check(cnt == cnt2)
        check(cnt2 == 20000)

        println("test timed sync, not timing out")
        var start = millinow()
        check(mutex.timedSync(10000) {
            true
        } != null)
        var end = millinow()
        check(end-start < 10000)

        println("test timed sync, timing out")
        mutex.lock {
            val t = Thread {
                var thstart = millinow()
                check(mutex.timedSync(200) {
                    check(false, {"you should never get this mutex!"})
                } == null)
                var thend = millinow()
                check(thend-thstart >= 200, {"timeout was too short!"} )
                println("timedSync timeout completed")
            }
            println("join")
            t.join()
        }
    }


    @Test
    fun testGate()
    {
        var done = false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -2
        val th = Thread {
            // println("thunk is running")
            while(processed < 50) {
                lock.wlock {
                    while (q.size > 0) {
                        val s = q.removeFirst()
                        //println("Consumed: $s")
                        processed = s
                        lock.wake()
                    }
                    if (processed < 50 && !done) await()
                }
            }
            // println("thread completed")
        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            done = true
            lock.wake()
        }

        th.join()
    }

    @Test
    fun testTimedGate()
    {
        val lock = Gate()

        // Test wait never given
        var start = millinow()
        val ret = lock.timedwaitfor(1500, { false }, { check(false) })  // never going to execute the block
        check(ret == null, { "ret is null!!"} )
        var end = millinow()
        check(end-start >= 500, { "elapsed time is ${end-start}."})  // At least the wait amount should have passed

        // Test lock never given
        lock.lock {
            val t = Thread {
                var threadSt = millinow()
                check(lock.timedwaitfor(500, { false }, { check(false)}) == null)
                var threadEnd = millinow()
                check(threadEnd-threadSt >= 500, {"duration was ${threadEnd-threadSt}"})  // At least the wait amount should have passed
            }
            t.join()
        }

    }

    @Test
    fun testSynchronizerWaitLoop()
    {
        done=false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -2
        val th = Thread {
            //println("thunk is running")
            lock.loopwhile({!done}) {
                while (q.size > 0)
                {
                    val s = q.removeFirst()
                    //println("Consumed: $s")
                    processed = s
                    lock.wake()
                }
            }

        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            done = true
            lock.wake()
        }

        th.join()
    }

    @Volatile
    var done = false
    @Test
    fun testSynchronizerWaitFor()
    {
        done = false
        val lock = Gate()
        val q = mutableListOf<Int>()
        var processed:Int = -1
        val th = Thread {
            // println("thunk is running")
            while(!done)
            {
                lock.waitfor({ q.size > 0 || done }) {
                    if (done) return@waitfor
                    while(q.size > 0)
                    {
                        val s = q.removeFirst()
                        // println("Consumed: $s")
                        processed = s
                        yield()
                        lock.wake()
                    }
                }
            }
        }

        for(i in 1..50)
        {
            lock.lock {
                q.add(i)
                lock.wake()
            }
            yield()
            //println("added $i")
        }

        lock.waitfor({ processed == 50 }) {
            //println("consumed all, setting done = true")
            done = true
            lock.wake()
        }

        //println("joining thread")
        th.join()
    }



    @Test
    fun repeatTest()
    {
        for(i in 0..10)
        {
            println("Iteration $i")
            testMutex()
            println("testLock")
            testGate()
            println("testLockWaitFor")
            testSynchronizerWaitFor()
            println("testLockWaitLoop")
            testSynchronizerWaitLoop()
        }
    }

    @Test fun leakyBucketTest()
    {
        val lb = LeakyBucket(50, 10, 20)
        try {
            lb.take(51)
            check(false)
        }
        catch (e: IllegalArgumentException)  // taking more than bucket max -> infinite wait
        {
            check(true)
        }

        val start = TimeSource.Monotonic.markNow()
        lb.take(10)
        val el = start.elapsedNow().inWholeMilliseconds
        check(el < 5, { "Expected approximately no elapsed time, got $el" } )
        lb.take(30)
        val elapsed = start.elapsedNow().inWholeMilliseconds
        check(elapsed > 1900 && elapsed < 4000, { "Expected approximately 2000 elapsed ms, got $elapsed"})  // total leaked 40, started at 20, filling 10 per sec, so 2 seconds wait

        check(lb.trytake(3000, { true }) == null)
        lb.stop()
        check(lb.trytake(3000, { true }) == true)
    }

    @Test
    fun testReadme()
    {
        // 1
        val th = Thread {
            println("running in a different thread")
        }
        val th1 = Thread("threadName") {
            println("running in a different thread")
        }

        // 2
        val th2 = Thread { millisleep(1000U) }
        th.join()

        //val th3 = Thread { millisleep(100000U) }
        //th.join(1000)  // wait for a second; if the thread hasn't completed yet, then throw an exception


        //3 Synchronize/Atomize access to data
        val mutex = Mutex()
        var a = 1
        var b = 2

        val c = mutex.lock {
            a = a+b
            b = b+a
            a+b
        }
        mutex.synchronized {  // same as lock, but follows Java naming
        }

        val d = mutex.ifavailable {
        }
        if (d == null)  // lock was taken, your block was NOT executed
        {}

        val e = mutex.trylock { }  // more traditional name for ifavailable

        val f = mutex.timedlock(1000) {}
        if (f == null)  // timeout, your block was NOT executed
        {}


        //4 Synchronize threads
        var done = false
        val gate = Gate()
        val q = mutableListOf<Int>()
        Thread {
            gate.loopwhile({!done})  // loop, waiting for some other thread to wake me (gate is locked, except when this thread is waiting)
            {
                while (q.size > 0)  // Always consume everything you can, because there's no guarantee that 1 call to the wake function == 1 wake up
                {
                    val s = q.removeFirst()
                    println(s.toString())
                }
            }
        }

        for(i in 1..50)
        {
            gate.synchronized {
                q.add(i)
                gate.wake()
            }
        }
        // ok all done so wake up
        gate.synchronized {
            done=true
            gate.wake()
        }
    }
}
